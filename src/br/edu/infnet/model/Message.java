package br.edu.infnet.model;

public class Message {
	private String dst;
	private String src;
	private String data;
	
	public String getSrc() {
		return src;
	}
	public String getData() {
		return data;
	}
	public String getDst() {
		return dst;
	}
	
	public Message (String src, String data, String dst) {
		this.src = src;
		this.data = data;
		this.dst = dst;
	}
}

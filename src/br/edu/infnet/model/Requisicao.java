package br.edu.infnet.model;

public class Requisicao {
	private String id;
	private String cmd;
	private int msgNr;
	private String data;

	public String getDst() {
		return dst;
	}

	private String dst;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Requisicao(String cmd) {
		this.cmd = cmd;
	}

	public String getId() {
		return id;
	}
	public String getCmd() {
		return cmd;
	}
	public int getMsgNr() {
		return msgNr;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public void setMsgNr(int msgNr) {
		this.msgNr = msgNr;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}
}

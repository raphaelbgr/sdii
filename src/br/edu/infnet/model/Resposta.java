package br.edu.infnet.model;

import java.util.ArrayList;
import java.util.List;

public class Resposta {
	private String id;
	private List<Message> data;
	private int msgNr;
	
	
	public String getId() {
		return id;
	}
	public void setData(List<Message> data) {
		this.data = data;
	}
	public void setMsgNr(int msgNr) {
		this.msgNr = msgNr;
	}
	public List<Message> getData() {
		return data;
	}
	public int getMsgNr() {
		return msgNr;
	}
	public void setId(String i) {
		id = i;
	}
	public void addData(String src, String msg) {
		if (data == null) { data = new ArrayList<Message>(); }
		data.add(new Message(src, msg, "0"));
	}

}

package br.edu.infnet.model;

import java.net.Socket;

public class Client {
	
	private Socket socket;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getMsgNr() {
		return msgNr;
	}
	public void setMsgNr(int msgNr) {
		this.msgNr = msgNr;
	}
	public Client(Socket s, String id, int msgNr) {
		this.id = id;
		this.msgNr = msgNr;
		this.socket = s;
	}
	
	public Socket getSocket() {
		return socket;
	}
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	private Socket s; // Ser[a usado para o servidor falar com ele mais tarde.
	private String id;
	private int msgNr;
}

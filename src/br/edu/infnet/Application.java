package br.edu.infnet;

import br.edu.infnet.server.ServerReceiverListener;
import br.edu.infnet.server.TcpServer;

public class Application {

	public static void main(String[] args) {
		TcpServer tcp = new TcpServer(3000);
		tcp.setListener(new ServerReceiverListener());
		tcp.startService();
	}

}

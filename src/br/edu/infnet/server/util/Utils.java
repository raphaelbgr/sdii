package br.edu.infnet.server.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

	public static String getTimestamp() {
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		String dateFormatted = formatter.format(new Date());
		return "[" + dateFormatted + "]";
	}
	
	public static void displayRemoteIp(int PORT) {
		Runnable r = () -> getRemoteIp(PORT);
		new Thread(r).start();
	}

	private static void getRemoteIp(int PORT) {
		URL whatismyip;
		try {
			whatismyip = new URL("http://checkip.amazonaws.com");
			BufferedReader in = new BufferedReader(new InputStreamReader(
					whatismyip.openStream()));
			String ip = in.readLine(); //you get the IP as a String
			
			System.out.println(Utils.getTimestamp()
					+ " SERVER> "
					+ ip + ":" + PORT + " TCP WAN");
		} catch (MalformedURLException e) {}
		catch (IOException e) {}
		
	}
}

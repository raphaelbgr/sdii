package br.edu.infnet.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.List;

import br.edu.infnet.central.ServerSender;
import br.edu.infnet.model.Resposta;
import com.google.gson.Gson;

import br.edu.infnet.central.ClientCentral;
import br.edu.infnet.model.Message;
import br.edu.infnet.model.Requisicao;
import br.edu.infnet.server.interfaces.OnServerReceive;
import br.edu.infnet.server.util.Utils;

public class ServerReceiverListener implements OnServerReceive {
	
	@Override
	public void onAcceptSocket(Socket s) {
		String origin = null;
		if (s.getInetAddress().getHostAddress().equalsIgnoreCase("127.0.0.1")) origin = " (Loopback)";
		else origin = s.getInetAddress().isLinkLocalAddress() ? " (WAN)" : " (LAN)";
			 
		System.out.println(Utils.getTimestamp()
				+ " SERVER> Conex�o estabelecida com o IP " + s.getInetAddress().getHostAddress()
				+ origin);
		
		// Delegate to another thread
		Runnable task = () -> { onWaiting(s); };
		new Thread(task).start();
	}
	
	@Override
	public void onWaiting(Socket s) {
		String json = null;
		BufferedReader br = null;
		while (true) {
			try {
				json = null;
				// Thread espera aqui
				br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				json = br.readLine();
				
				// Protocolo do servidor
				Requisicao r = onProcessJson(json);
				Resposta respObj = onProcessCommand(s, r);

				String resp;
				if (respObj != null)
					resp = new Gson().toJson(respObj, Resposta.class);
				else
					resp = new ServerResponseBuilder(r).buildResponse();

				onRespondToClient(s, r, resp);
				
				// Mensagem redirecionada - dst = fulano
//				onRedirectToClient(r, resp);
				
				// Mensagem Broadcast, dst = all
				onBroadCast(s, r, new ServerResponseBuilder(r).buildBroadcast());
				
				// Logar no console
				System.out.println(Utils.getTimestamp() + " SERVER <<< " + json);
			} catch (Exception e) {
				breakSocket(s, br, null);
				printError(e, s, json);
				break;
			}
		}
	}
//	private void onRedirectToClient(Requisicao r, String resp) {
//		if (r.getDst() != null) {
//			Socket s = ClientCentral.getInstance().getClientSocketByUsername(r.getDst());
//			List<Socket> list = new ArrayList<>();
//			list.add(s);
//
//			new ServerSender(list, r, resp);
//		}
//	}

	private void onBroadCast(Socket s, Requisicao r, String resp) {
		if (r.getDst() != null || r.getId() != null) {
			List<Socket> ss = ClientCentral.getInstance().getAllClientSockets();
			new ServerSender(ss, r, resp);
		}
	}

	private void printError(Exception e, Socket s, String json) {
		System.err.println(Utils.getTimestamp() + " SERVER> "
		+ s.getInetAddress().getHostAddress() + " Desconectado.");
	}
	
	private void breakSocket(Socket s, BufferedReader br, OutputStreamWriter out) {
		try {
			s.close();
			if (br != null)
				br.close();
			if (out != null)
				out.close();
		} catch (IOException e) {
			System.err.println(Utils.getTimestamp() + " SERVER> " + e.getLocalizedMessage());
		} finally {
			s = null;
		}
	}
	
	@Override
	public Requisicao onProcessJson(String json) {
		return new Gson().fromJson(json, Requisicao.class);
	}
	
	@Override
	public Resposta onProcessCommand(Socket s, Requisicao r) {
		Resposta resp = new Resposta();

		if (r.getCmd().equalsIgnoreCase("login")) {
			ClientCentral.getInstance().addClient(s, r);
		} else if (r.getCmd().equalsIgnoreCase("enviar")) {
			System.out.println(Utils.getTimestamp() + " " + r.getId() + " -> " + r.getDst()
			+ "> " + r.getData());
			ClientCentral.getInstance().getMessageDataset()
				.add(new Message(r.getId(), r.getData(), r.getDst()));
		} else if (r.getCmd().equalsIgnoreCase("receber")) {
			System.out.println(Utils.getTimestamp() + " " + r.getId()
			+ "> (receber)");

			resp.setData(ClientCentral.getInstance().getMessagesForUser(r.getId()));
			resp.setId(r.getId());
			resp.setMsgNr(ClientCentral.getInstance().getDatasetSize());

			return resp;
		} else if (r.getCmd().equalsIgnoreCase("logoff")) {
			ClientCentral.getInstance().removeClient(r);
			System.out.println(Utils.getTimestamp() + " SERVER>" + r.getId()
			+ "> (logoff)");
		}

		return null;
	}
	
	@Override
	public void onRespondToClient(Socket s, Requisicao r, String json) {
		OutputStreamWriter out = null;
		try {
			System.out.println(Utils.getTimestamp() + " SERVER >>> " + json);
			out = new OutputStreamWriter(s.getOutputStream(),
					StandardCharsets.UTF_8);
			out.write(json + "\n");
			out.flush();
		} catch (Exception e) {
			System.err.println(Utils.getTimestamp() + " SERVER> " + e.getLocalizedMessage());
			breakSocket(s, null, out);
		}
	}
	
}

package br.edu.infnet.server;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import br.edu.infnet.central.ClientCentral;
import br.edu.infnet.model.Message;
import br.edu.infnet.model.Requisicao;
import br.edu.infnet.model.Resposta;
import br.edu.infnet.server.util.Utils;

public class ServerResponseBuilder {

	private Requisicao req;

	public ServerResponseBuilder(Requisicao r) {
		req = r;
	}

	public String buildResponse() {
		Resposta r = new Resposta();
		if (req.getCmd().equalsIgnoreCase("login")) {
			r.setId("0");
			r.setMsgNr(ClientCentral.getInstance().getDatasetSize());
			r.setData(ClientCentral.getInstance().getMessageDataset());
			return new Gson().toJson(r, Resposta.class);
		} else if (req.getCmd().equalsIgnoreCase("enviar")) {
			r.setId("0");
			r.setMsgNr(ClientCentral.getInstance().getDatasetSize());
			return new Gson().toJson(r, Resposta.class);
		} else if (req.getCmd().equalsIgnoreCase("logoff")) {
			r.setId("0");
			r.setMsgNr(ClientCentral.getInstance().getDatasetSize());
			return new Gson().toJson(r, Resposta.class);
		} else if (req.getCmd().equalsIgnoreCase("receber")) {
			r.setId("0");
			r.setData(ClientCentral.getInstance().getMessagesForUser(req.getDst()));
			r.setMsgNr(ClientCentral.getInstance().getDatasetSize());
			return new Gson().toJson(r, Resposta.class);
		} else {
			r.setId("0");
			r.addData("Server", Utils.getTimestamp() + " SERVER> Comando n�o reconhecido pelo servidor.");
			return new Gson().toJson(r, Resposta.class);
		}
	}
	
	/**
	 * Transforma uma requisi��o em uma resposta.
	 * @return String JSON
	 */
	public String buildBroadcast() {
		if (req.getData() != null && !(req.getData() instanceof String)) {
			List<Message> data = new ArrayList<>();
			for (Message message : data)
				{data.add(message);}
			
			Resposta r = new Resposta();
			r.setId(req.getId());
			r.setData(data);
			r.setMsgNr(ClientCentral.getInstance().getDatasetSize());
			return new Gson().toJson(r, Resposta.class);
		} return new Gson().toJson(req, Requisicao.class);
	}

}

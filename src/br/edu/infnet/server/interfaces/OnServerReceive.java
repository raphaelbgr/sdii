package br.edu.infnet.server.interfaces;

import java.net.Socket;

import br.edu.infnet.model.Requisicao;
import br.edu.infnet.model.Resposta;

public interface OnServerReceive {
	void onAcceptSocket(Socket s);
	void onWaiting(Socket s);
	Requisicao onProcessJson(String json);
	Resposta onProcessCommand(Socket s, Requisicao cmd);
	void onRespondToClient(Socket s, Requisicao r, String json);
}

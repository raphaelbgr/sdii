package br.edu.infnet.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import br.edu.infnet.server.util.Utils;

public class TcpServer {
	private int PORT;
	private ServerReceiverListener listener;
	
	public TcpServer(int p) {
		PORT = p;
	}

	public void startService() {
		Runnable task2 = () -> receiveConenctionAsync();
		new Thread(task2).start();;
	}
	
	@SuppressWarnings("resource")
	private void receiveConenctionAsync() {
		ServerSocket ss = null;
		printStatus();
		while(true) {
			try {
				ss = ss == null ? new ServerSocket(PORT) : ss;
				Socket s = ss.accept();
				listener.onAcceptSocket(s);
			} catch (IOException e) {
				System.err.println(Utils.getTimestamp() + " SERVER> " + e.getLocalizedMessage());
			}
		}
	}
	
	private void printStatus() {
		Utils.displayRemoteIp(PORT);
		String lan = "";
		String lo = "";
		try {
			lan = InetAddress.getLocalHost().getHostAddress();
			lo = InetAddress.getLoopbackAddress().getHostAddress();
		} catch (UnknownHostException e) {
			System.out.println(Utils.getTimestamp()
					+ " SERVER> Waiting for connection on port" + PORT + " (TCP)");
		}
		System.out.println(Utils.getTimestamp() + " SERVER> Listening on...");
		System.out.println(Utils.getTimestamp()
				+ " SERVER> "
				+ lo + ":" + PORT + " TCP Loopback");
		System.out.println(Utils.getTimestamp()
				+ " SERVER> "
				+ lan + ":" + PORT + " TCP LAN");
	}

	public void setListener(ServerReceiverListener serverReceiverListener) {
		listener = serverReceiverListener;
	}

}

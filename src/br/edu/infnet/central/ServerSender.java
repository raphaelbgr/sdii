package br.edu.infnet.central;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;

import br.edu.infnet.model.Requisicao;
import br.edu.infnet.server.util.Utils;

public class ServerSender {

	public ServerSender(List<Socket> ss, Requisicao r, String resp) {
		for (Socket socket : ss) {
			OutputStreamWriter out;
			try {
				out = new OutputStreamWriter(
						socket.getOutputStream());
				out.write(resp + "\n");
				out.flush();
			} catch (IOException e) {
				System.err.println(Utils.getTimestamp() + " SERVER> Failed to broadcast to: " +
						socket.getInetAddress().getHostAddress().toString());
			}
			
		}
	}
	
}

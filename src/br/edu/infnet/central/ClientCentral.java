package br.edu.infnet.central;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import br.edu.infnet.model.Client;
import br.edu.infnet.model.Message;
import br.edu.infnet.model.Requisicao;
import br.edu.infnet.server.util.Utils;

public class ClientCentral {
	private static ClientCentral instance;
	private Map <String, Client> onlineClients = new ConcurrentHashMap<> ();
	private List<Message> messageList = Collections.synchronizedList(new ArrayList<Message>());
	
	public void addClient(Socket s, Requisicao r) {
		onlineClients.put(r.getId(), new Client(s, r.getId(), r.getMsgNr()));
	}
	
	public void removeClient(Requisicao r) {
		onlineClients.remove(r.getId());
	}

	public int getDatasetSize() {
		return messageList.size();
	}

	public List<Message> getMessageDataset() {
		return messageList;
	}
	
	/**
	 * Rtorna uma c�pia da lista de sockets dos clients contectados.
	 * @return List<Socket>
	 */
	public List<Socket> getAllClientSockets() {
		List<Socket> socketList = new ArrayList<>();
		Iterator<Entry<String, Client>> it = onlineClients.entrySet().iterator();
	    while (it.hasNext()) {
	        Entry<String, Client> pair = it.next();
	        socketList.add(pair.getValue().getSocket());
	    }
		return socketList;
	}
	
	public List<String> getOnlineClientList() {
		List<String> onlineList = new ArrayList<>();
		Iterator<Entry<String, Client>> it = onlineClients.entrySet().iterator();
		String onlinePplToString = "";
	    while (it.hasNext()) {
	        Entry<String, Client> pair = it.next();
	        onlineList.add(pair.getKey().toString());
	        onlinePplToString = onlinePplToString + " " + pair.getKey().toString();
	    }
	    System.out.println(Utils.getTimestamp() + " SERVER> Pessoas online:");
		return onlineList;
	}
	
	// Singleton
	private ClientCentral(){}
	public static ClientCentral getInstance() {
		if (instance == null) instance = new ClientCentral();
		return instance;
	}

	public Socket getClientSocketByUsername(String dst) {
		if (dst != null)
			return onlineClients.get(dst).getSocket();
		else
			return null;
	}

	public List<Message> getMessagesForUser(String dst) {
		List<Message> list = new ArrayList<>();

		if (dst != null) {
			for (Message m : messageList) {
				if (m.getDst().equalsIgnoreCase(dst)) {
					list.add(m);
				} else if (m.getSrc().equalsIgnoreCase(dst)) {
					list.add(m);
				}
			}
		}
		return list;
	}
}
